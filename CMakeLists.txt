### Maintainer: sschulz,gwalck,flier[@techfak.uni-bielefeld.de] ###
cmake_minimum_required(VERSION 2.8)
PROJECT(rise-naoqi-startup)
SET(CMAKE_BUILD_TYPE distribution)
set(RISE_STARTUP_VERSION 0.1.0)
set(ROS_DISTRO $ENV{ROS_DISTRO})
set(ROS_PYTHON_VERSION $ENV{ROS_PYTHON_VERSION})

add_subdirectory(vdemo)
