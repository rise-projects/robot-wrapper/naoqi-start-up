# VDEMO Startup for NaoQi Distribution

The vdemo startup is a versatile component designed to simplify the management of ROS nodes. In this repository, you will find the configuration files for the [vdemo](https://code.cor-lab.de/projects/vdemo/repository) interface, which facilitates the process of initiating and halting individual ROS nodes that have been pre-configured. With this tool, you can streamline the control of your ROS-based applications, making it easier to manage the individual components.

The startup components refers to the use of the robots NAO and Pepper from [Aldebaran (Softbanks)](https://www.aldebaran.com/de/pepper-and-nao-robots-education).

## VDEMO-Controller

To run own ROS nodes in the created environment, open a SSH connection by the interface (black arrow) and start your custom script.

<img src="https://gitlab.ub.uni-bielefeld.de/rise-projects/rise-documentation/-/raw/development/source/images/vdemo_open_terminal.svg?ref_type=heads" height="400" />